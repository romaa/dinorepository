package game.score;

import game.speed.SpeedManager;

/**
 * This class manage part of calculation of score.
 * @author ROMANELLI AURORA
 *
 */
public class MyScoreManager implements ScoreManager {

  private Integer score;
  private int steps;
  private final SpeedManager speed;
  RankingManager ranking = new RankingManager();
  
  /**
   * Constructor.
   * @param speed for calculation because are connected to the speed
   */
  public MyScoreManager(final SpeedManager speed) {
    this.score = 0;
    this.steps = 0;
    this.speed = speed;
  }
  

  @Override
  public Integer getScore() {
    return score;
  }
  

  @Override
  public void incrementScore() {
    if (steps >= 32 / (int)speed.getSpeed()) {
      score++;
      steps = 0;
    } else {
      steps++;
    } 
  }
    
  @Override
  public void reset() {
    this.score = 0;
    this.steps = 0;
  }
  
  /**
   * getter of ranking.
   * @return ranking
   */
  public RankingManager getRanking() {
    return ranking;
  }


  /**
   * Method that add the player in the ranking.
 * @param name of the player.
 */
  public void addScore(final String name) {
    ranking.addScore(name,this.score);      
  }

}
