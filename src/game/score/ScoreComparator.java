package game.score;

import java.util.Comparator;

/**
 * Class to compare the score values to create the ranking.
 * @author ROMANELLI AURORA
 *
 */
public class ScoreComparator implements Comparator<HighScore> {

  @Override
  public int compare(final HighScore score1, final HighScore score2) {

    final int sc1 = score1.getScore();
    final int sc2 = score2.getScore();

    if (sc1 > sc2) {
      return -1; 
    } else if (sc1 < sc2) {
      return +1;
    } else {
      return 0;
    }
  }

}
