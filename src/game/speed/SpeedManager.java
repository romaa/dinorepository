package game.speed;

/**
 * Interface for the two types of speed.
 * @author ROMANELLI AURORA 
 */
public interface SpeedManager {

  /** 
   * getter.
   * @return Speed 
   */
  public double getSpeed();

  /**
   * Update the speed.
   */
  public void updateSpeed();

  /**
  * Reset the speed manager parameters.
  */
  void reset();


}
