package game.environment;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;

/**
 * This class create and manage all type of lands in the game.
 * 
 * @author POGGI GIOVANNI
 */
public class Land extends Ambientation {

  /**
   * This field is used to save the vertical position of the land.
   */
  private static final int POSY_OF_LAND = 103;
  /**
   * This field is used to save in a list all type of game's land.
   */
  private List<ImageLand> listOfAllLand;
  /**
   * This field is used to save the first sprite of land.
   */
  private BufferedImage firstSpriteOfLand;
  /**
   * This field is used to save the second sprite of land.
   */
  private BufferedImage secondSpriteOfLand;
  /**
   * This field is used to save the third sprite of land.
   */
  private BufferedImage thirdSpriteOfLand;
  /**
   * This field is used to choose one random number from 1 to 3.
   */
  private final int seed = 3;
  
  /**
   * Prepare the fields to use and set the all type of images' land.
   * @param width the width of the game.
   */
  public Land(final int width) {
    try {
      firstSpriteOfLand = ImageIO.read(this.getClass()
            .getResourceAsStream("/game/environment/land1.png"));
      secondSpriteOfLand = ImageIO.read(this.getClass()
            .getResourceAsStream("/game/environment/land2.png"));
      thirdSpriteOfLand = ImageIO.read(this.getClass()
            .getResourceAsStream("/game/environment/land3.png"));
    } catch (IOException e) {
      System.out.println("Land .png Error: " + e.getMessage());
    }
    int numberOfLand = width / firstSpriteOfLand.getWidth() + 2;
    listOfAllLand = new ArrayList<ImageLand>();
    for (int i = 0; i < numberOfLand; i++) {
      ImageLand imageLand = new ImageLand();
      imageLand.positionX = i * firstSpriteOfLand.getWidth();
      setLand(imageLand);
      listOfAllLand.add(imageLand);
    }
  }

  /**
   * This method choose randomly what type of land will be insert in the game.
   * @return number of the type of land to use.
   */
  private int getTypeOfLand() {
    Random rand = new Random();
    int landToChoose = rand.nextInt(seed);
    switch (landToChoose) {
      case 0:
        return 1;
      case 1:
        return 2;
      default:
        return seed;
    }
  }

  /**
   * This method set one of the all type of lands in the environment.
   * @param imgLand what image of the land will be use in the game
   */
  private void setLand(final ImageLand imgLand) {
    int type = getTypeOfLand();
    switch (type) {
      case 1:
        imgLand.image = firstSpriteOfLand;
        break;
      case 2:
        imgLand.image = secondSpriteOfLand;
        break;
      default:
        imgLand.image = thirdSpriteOfLand;
        break;
    }
  }

  /**
   * This class describe how is made the object of imageland in the game.
   * 
   * @author POGGI GIOVANNI
   */
  private class ImageLand {
    /**
     * This field set x position of the cloud in the sky.
     */
    private float positionX;
    /**
     * This field save the image of the cloud.
     */
    private BufferedImage image;
  }

  @Override
  public void update(final double speed) {
    Iterator<ImageLand> itr = listOfAllLand.iterator();
    ImageLand firstElement = itr.next();
    firstElement.positionX -= speed;
    float previousPosX = firstElement.positionX;
    while (itr.hasNext()) {
      ImageLand anotherLand = itr.next();
      anotherLand.positionX = previousPosX + firstSpriteOfLand.getWidth();
      previousPosX = anotherLand.positionX;
    }
    if (firstElement.positionX < -firstSpriteOfLand.getWidth()) {
      listOfAllLand.remove(firstElement);
      firstElement.positionX = previousPosX + firstSpriteOfLand.getWidth();
      setLand(firstElement);
      listOfAllLand.add(firstElement);
    }
  }

  @Override
  public void drawEnvironment(final Graphics env) {
    for (ImageLand imgLand : listOfAllLand) {
      env.drawImage(imgLand.image, (int) imgLand.positionX, POSY_OF_LAND, null);
    }
  }
}