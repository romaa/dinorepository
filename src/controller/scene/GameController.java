package controller.scene;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import view.sceneLoader.FXMLPath;

/**
 * This class manage the scene Game of the game.
 * 
 * @author POGGI GIOVANNI
 */
public class GameController implements Initializable {

  /**
   * Fields to allow user to move window of the game.
   */
  private double xoffset;
  private double yoffset;
  /**
   * Field of the scene's Panel.
   */
  @FXML
  private Pane rootPane;

  /**
   * This method open the scene High Score when user press the button High Score.
   * 
   * @param event of the button.
   * @throws IOException if the method fails to load scene.
   */
  @FXML
  private void pressHighScore(ActionEvent event) throws IOException {
    this.rootPane.getChildren().clear();
    Parent root = FXMLLoader.load(getClass().getResource(FXMLPath.HIGHSCORE.getPath()));
    this.rootPane.getChildren().add(root);
  }

  /**
   * This method open the game when user press the button Start Game.
   * 
   * @param event of the button.
   * @throws IOException if the method fails to load scene.
   */
  @FXML
  private void pressStartGame(ActionEvent event) throws IOException {
    this.rootPane.getChildren().clear();
    Parent root = FXMLLoader.load(getClass().getResource(FXMLPath.MODE.getPath()));
    this.rootPane.getChildren().add(root);
  }

  /**
   * This method open the scene How To Play when user press the button How To Play.
   * 
   * @param event of the button.
   * @throws IOException if the method fails to load scene.
   */
  @FXML
  private void pressHowToPlay(ActionEvent event) throws IOException {
    this.rootPane.getChildren().clear();
    Parent root = FXMLLoader.load(getClass().getResource(FXMLPath.HOWTOPLAY.getPath()));
    this.rootPane.getChildren().add(root);
  }

  /**
   * This method close the game if user press exit.
   * 
   * @param event of the button.
   */
  @FXML
  private void pressCloseBtn(ActionEvent event) {
    System.exit(0);
  }

  @Override
  public void initialize(URL arg0, ResourceBundle arg1) {
    this.rootPane.setOnMousePressed(event -> {
      this.xoffset = this.rootPane.getScene().getWindow().getX() - event.getScreenX();
      this.yoffset = this.rootPane.getScene().getWindow().getY() - event.getScreenY();
    });
    this.rootPane.setOnMouseDragged(event -> {
      this.rootPane.getScene().getWindow().setX(event.getScreenX() + this.xoffset);
      this.rootPane.getScene().getWindow().setY(event.getScreenY() + this.yoffset);
    });
  }
}